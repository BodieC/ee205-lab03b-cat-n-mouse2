#!/bin/bash
## @author Bodie Collins <bodie@hawaii.edu>
## @date 01 Jan 2022
##
##
DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF


#variable to make while loop run
go=1

while [ $go == 1 ]
do
   echo -n OK cat, I\'m thinking of number from 1 to $THE_MAX_VALUE. Make a Guess:
   #store guess in variable
   read guess

   #if the guess is zero or negative
   if [ $guess -le 0 ]
   then
   
      echo You must enter a number that\'s greater than 0
   
   #if the guess is larger than maxvalue
   elif [ $guess -gt $THE_MAX_VALUE ]
   then
      echo You must enter a number that is less than or equal to $THE_MAX_VALUE
   
   #if the number is correct range and guess is to large
   elif [ $guess -gt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo No cat the number I\'m thinking of is smaller than $guess

   #if the number is in correct range and to small
   elif [ $guess -lt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo No cat the number I\'m thinking if is larger than $guess

   #if the number matches picked number
   elif [ $guess -eq $THE_NUMBER_IM_THINKING_OF ]
   then
      echo You got me
      echo  \|\\_\|\\  
      echo  \|\_ \_\|
      echo  \( T   \)  
      echo   MEOW
      
      #end while loop ending game
      go=0
   fi   

done
